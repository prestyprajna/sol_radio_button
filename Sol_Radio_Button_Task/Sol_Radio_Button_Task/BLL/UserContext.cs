﻿using Sol_Radio_Button_Task.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Sol_Radio_Button_Task.Models;

namespace Sol_Radio_Button_Task.BLL
{
    public class UserContext: UserDAL
    {
        public int CalculateAge(string date)
        {
            var currentYear = DateTime.Now.Year;

            var getYear = Convert.ToInt32(Regex.Split(date, "-")[0]);

            var ageCalculate = currentYear - getYear;

            return ageCalculate;
        }

        public override string SetUserData(UserEntity userEntityObj)
        {
            return base.SetUserData(userEntityObj);
        }
    }
}