﻿using Sol_Radio_Button_Task.DAL.ORD;
using Sol_Radio_Button_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Radio_Button_Task.DAL
{
    public class UserDAL
    {
        #region  declaration
        private UserDcDataContext _dc = null;
        #endregion

        #region  constructor
        public UserDAL()
        {
            _dc = new UserDcDataContext();
        }
        #endregion

        #region  public methods

        public List<CityEntity> GetCityData(CityEntity cityEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery = _dc?.uspGetCity(
                 "GetCityData",
                 cityEntityObj?.CityId,
                 cityEntityObj?.CityName,
                 ref status,
                 ref message
                 )
                 ?.AsEnumerable()
                 ?.Select((leCityObj) => new CityEntity()
                 {
                     CityId = leCityObj?.CityId,
                     CityName = leCityObj?.CityName
                 })
                 ?.ToList();

            return getQuery;

        }

        public virtual string SetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var setQuery =
                _dc?.uspSetUser(
                    "INSERT",
                    userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    Convert.ToDateTime(userEntityObj?.BirthDate),
                    userEntityObj?.Age,
                    userEntityObj?.Gender,
                    userEntityObj?.CityId,
                    ref status,
                    ref message
                    );

            return message;
        }

        public decimal GetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery =
                _dc?.uspGetUser(
                    "SELECT",
                   userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    //Convert.ToDateTime(userEntityObj?.BirthDate),
                    //userEntityObj?.Age,
                    //userEntityObj?.Gender,
                    //userEntityObj?.CityId,                    
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leEntityObj) => new UserEntity()
                    {
                        UserId = leEntityObj?.UserId
                        //FirstName = leEntityObj?.FirstName,
                        //LastName = leEntityObj?.LastName,
                        //BirthDate = Convert.ToString(leEntityObj?.BirthDate),
                        //Age = leEntityObj?.Age,
                        //Gender = leEntityObj?.Gender,
                        //CityId = leEntityObj?.CityId
                    })
                    ?.FirstOrDefault()
                    ?.UserId;
                   

            return Convert.ToDecimal(getQuery);
        }

        public string SetHobbiesData(List<HobbyEntity> hobbyEntityObj)
        {
            int? status = null;
            string message = null;

            foreach (HobbyEntity val in hobbyEntityObj)
            {
                var setQuery = _dc?.uspSetHobbies(
              "INSERT",
              val?.HobbyId,
              val?.HobbyName,
              val?.Interested,
              val?.UserId,
              ref status,
              ref message
              );
            }

            return message;
        }

        public UserEntity GetUserDataByID(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var getQuery =
                _dc?.uspGetUser(
                    "SELECT_USER_DATA_BY_ID",
                     userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    //Convert.ToDateTime(userEntityObj?.BirthDate),
                    //userEntityObj?.Age,
                    //userEntityObj?.Gender,
                    //userEntityObj?.CityId,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leEntityObj) => new UserEntity()
                    {
                        UserId = leEntityObj?.UserId,
                        FirstName = leEntityObj?.FirstName,
                        LastName = leEntityObj?.LastName,
                        BirthDate = Convert.ToString(leEntityObj?.BirthDate),
                        Age = leEntityObj?.Age,
                        Gender = leEntityObj?.Gender,
                        CityId = leEntityObj?.CityId

                    })
                   ?.FirstOrDefault();       

            return getQuery;
        }       

        public List<HobbyEntity> GetHobbiesDataById(HobbyEntity hobbyEntityObj)
        {
            int? status = null;
            string message = null;


            var getQuery = _dc?.uspGetHobbies(
                "SELECT",
                hobbyEntityObj?.HobbyId,
                hobbyEntityObj?.HobbyName,
                hobbyEntityObj?.Interested,
                hobbyEntityObj?.UserId,
                ref status,
                ref message
                )
                ?.AsEnumerable()
                ?.Select((leListHobbiesObj) => new HobbyEntity()
                {
                    HobbyId = leListHobbiesObj?.HobbyId,
                    HobbyName = leListHobbiesObj?.HobbyName,
                    Interested = Convert.ToBoolean(leListHobbiesObj?.Interested),
                    UserId = leListHobbiesObj?.UserId
                })
                ?.ToList();

            return getQuery;
        }

        //public List<HobbyEntity> GetHobbiesData(HobbyEntity hobbyEntityObj)
        //{
        //    int? status = null;
        //    string message = null;


        //    var getQuery = _dc?.uspGetHobbies(
        //        "SELECT",
        //        hobbyEntityObj?.HobbyId,
        //        hobbyEntityObj?.HobbyName,
        //        hobbyEntityObj?.Interested,
        //        hobbyEntityObj?.UserId,
        //        ref status,
        //        ref message
        //        )
        //        ?.AsEnumerable()
        //        ?.Select((leListHobbiesObj) => new HobbyEntity()
        //        {
        //            HobbyId = leListHobbiesObj?.HobbyId,
        //            HobbyName = leListHobbiesObj?.HobbyName,
        //            Interested = Convert.ToBoolean(leListHobbiesObj?.Interested),
        //            UserId = leListHobbiesObj?.UserId
        //        })
        //        ?.ToList();

        //    return getQuery;
        //}

        #endregion
    }
}