﻿using Sol_Radio_Button_Task.DAL;
using Sol_Radio_Button_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_Radio_Button_Task.User.Edit
{
    public partial class UpdatePage
    {
        #region  Property
        private string FirstNameBinding
        {
            get  //UI to entity
            {
                return txtFirstName.Text;
            }
            set  //entity to UI
            {
                txtFirstName.Text = value;
            }
        }

        private string LastNameBinding
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }      

        private string BirthDateBinding
        {
            get
            {
                return txtBirthDate.Text;
            }
            set
            {
                txtBirthDate.Text = value;
            }
        }

        private int AgeBinding
        {
            get
            {
                return Convert.ToInt32(lblAge.Text);
            }
            set
            {
                lblAge.Text = Convert.ToString(value);
            }
        }

        private bool GenderBinding
        {
            get
            {
                return (rbGender?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.FirstOrDefault((leListObj) => leListObj.Selected == true)
                    ?.Text) == "Male" ? true : false;
            }
            set
            {
                bool flag = value;

                rbGender?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.ForEach((leListObj) =>
                    {
                        if (flag == true)
                        {
                            if (leListObj.Text == "Male")
                            {
                                leListObj.Selected = true;
                            }
                        }
                        else
                        {
                            if(leListObj.Text=="Female")
                            {
                                leListObj.Selected = true;
                            }
                        }
                    });
            }
        }

        public dynamic CityBinding
        {
            get
            {
                return Convert.ToInt32(ddlCity.SelectedItem.Value);
                //return ddlCity.SelectedValue;
            }
            set
            {
                List<CityEntity> listCityEntityObj = value as List<CityEntity>;
                //ddlCity.SelectedItem.Value = Convert.ToString(value);
            }
        }

        private List<HobbyEntity> HobbyBinding
        {
            get
            {
                var result = new UserDAL().GetUserData(this.DataMapping());

                return
                    chkHobbies
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Select((leListObj) => new HobbyEntity()
                    {
                        HobbyId = Convert.ToDecimal(leListObj?.Value),
                        HobbyName = leListObj?.Text,
                        Interested = Convert.ToBoolean(leListObj?.Selected),
                        //UserId = Convert.ToDecimal(leListObj?.Value)
                        UserId = result

                    })
                    ?.ToList();
            }
            set
            {
                chkHobbies.DataSource = value;
                chkHobbies.DataBind();

                foreach (ListItem listItemObj in chkHobbies.Items)
                {
                    listItemObj.Selected = Convert.ToBoolean(value
                        ?.AsEnumerable()
                        ?.Where((leUserEntityObj) => leUserEntityObj.HobbyName == listItemObj.Text)
                        ?.FirstOrDefault()
                        ?.Interested);

                }
            }
        }

        private decimal SearchTextBoxBinding
        {
            get
            {
                return Convert.ToDecimal(txtSearchById.Text);
            }
            set
            {
                txtSearchById.Text = Convert.ToString(value);
            }
        }
        #endregion

    }
}