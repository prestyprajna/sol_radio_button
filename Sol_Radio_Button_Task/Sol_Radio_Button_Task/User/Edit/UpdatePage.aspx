﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdatePage.aspx.cs" Inherits="Sol_Radio_Button_Task.User.Edit.UpdatePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>

            <tr>
                <td>
                    <asp:TextBox ID="txtSearchById" runat="server" placeholder="Enter ID"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                </td>
            </tr>

             <tr>
                        <td> 
                            <asp:Label ID="lblHobbies"  runat="server"  Text="Hobbies : "></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBoxList ID="chkHobbies" runat="server">
                                <asp:ListItem Text="Drawing" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Reading Novels" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Gardening" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Cricket" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Cooking" Value="5"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>                   

                    <tr>
                        <td>
                            <asp:TextBox ID="txtBirthDate" runat="server" TextMode="Date"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblAge" runat="server"></asp:Label>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblGender" runat="server" Text="Gender :"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbGender" runat="server">
                                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>    
                    </tr>

                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>
                        </td>
                    </tr>

            <tr>
                <td>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                </td>
            </tr>

        </table>    
    </div>
    </form>
</body>
</html>
