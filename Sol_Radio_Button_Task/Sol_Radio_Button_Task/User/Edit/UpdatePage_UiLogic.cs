﻿using Sol_Radio_Button_Task.DAL;
using Sol_Radio_Button_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_Radio_Button_Task.User.Edit
{
    public partial class UpdatePage
    {
        public void CityDataBinding()
        {
            var cityEntityObj = new UserDAL().GetCityData(null);

            // ddlCity.DataSource = null;
            //ddlCity.Items.Clear();

            ddlCity.DataSource = cityEntityObj;
            ddlCity.DataBind();

            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("--select city--", "0"));
            ddlCity.SelectedIndex = 0;

        }

        /// <summary>
        /// UI to Entity
        /// </summary>
        /// <returns></returns>
        private UserEntity DataMapping()
        {
            var userEntityObj = new UserEntity()
            {
                FirstName = this.FirstNameBinding,
                LastName = this.LastNameBinding,
                BirthDate = Convert.ToString(this.BirthDateBinding),
                Age = this.AgeBinding,
                Gender = this.GenderBinding,
                CityId = this.CityBinding
            };

            return userEntityObj;
        }


        /// <summary>
        /// Entity to UI
        /// </summary>
        /// <param name="userEntityObj"></param>
        private void DataMappingEntityToUi()
        {
            

            UserEntity userEntityObj = this.GetUserData();

            //this.SearchTextBoxBinding = Convert.ToDecimal(userEntityObj?.UserId);
            this.FirstNameBinding = userEntityObj?.FirstName;
            this.LastNameBinding = userEntityObj?.LastName;
            this.BirthDateBinding = Convert.ToString(userEntityObj?.BirthDate);
            this.AgeBinding = Convert.ToInt32(userEntityObj?.Age);
            this.GenderBinding =Convert.ToBoolean(userEntityObj?.Gender);
            this.CityBinding = Convert.ToInt32(userEntityObj?.CityId);

            List<HobbyEntity> resultHobbyEntityObj = this.GetHobbiesData();
            this.HobbyBinding = resultHobbyEntityObj;
        }

        public UserEntity GetUserData()
        {
            UserEntity resultUserEntityObj = new UserDAL().GetUserDataByID(new UserEntity()
            {
                UserId = this.SearchTextBoxBinding
            });

            return resultUserEntityObj;
        }

        public List<HobbyEntity> GetHobbiesData()
        {
            List<HobbyEntity> hobbyEntityObj=new UserDAL().GetHobbiesDataById(new HobbyEntity()
            {
                UserId=this.SearchTextBoxBinding
            });

            return hobbyEntityObj;
        }
    }
}