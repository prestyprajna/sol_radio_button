﻿using Sol_Radio_Button_Task.BLL;
using Sol_Radio_Button_Task.DAL;
using Sol_Radio_Button_Task.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_Radio_Button_Task.User.Add
{
    public partial class InsertPage
    {
        #region  declaration
        private UserContext userContextObj = null;
        #endregion

        public void CityDataBinding()
        {
            var cityEntityObj = new UserDAL().GetCityData(null);

            ddlCity.DataSource = null;
            ddlCity.Items.Clear();

            ddlCity.DataSource = cityEntityObj;
            ddlCity.DataBind();

            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("--select city--", "0"));
            ddlCity.SelectedIndex = 0;

        }

        private UserEntity DataMapping()
        {
            var userEntityObj = new UserEntity()
            {
                FirstName = this.FirstNameBinding,
                LastName = this.LastNameBinding,
                BirthDate = this.BirthDateBinding,
                Age = this.AgeBinding,
                Gender = this.GenderBinding,
                CityId = this.CityBinding
            };

            return userEntityObj;


        }

        private void AddUserData()
        {
            userContextObj = new UserContext();

            userContextObj.SetUserData(this.DataMapping());
        }

        private void BindAgeCalculation()
        {
            // Create an Instance of User ContextObj
            var userContextObj = new UserContext();

            // get calculate Age
            var age = userContextObj.CalculateAge(this.BirthDateBinding);

            // Bind age
            this.AgeBinding = age;
        }

        //private HobbyEntity HobbiesDataMapping()
        //{
        //    var result = new UserDAL().GetUserData(this.DataMapping());

        //    var hobbiesEntityObj = new HobbyEntity()
        //    {
        //        HobbyName = chkHobbies.SelectedItem.Text,
        //        Interested=chkHobbies.SelectedItem.Selected,
        //        UserId = result
        //    };
        //    return hobbiesEntityObj;
        //}

        private void HobbiesDataBinding()
        {     
            new UserDAL().SetHobbiesData(this.HobbyBinding);
        }

    }
}