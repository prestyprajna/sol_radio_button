﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Radio_Button_Task.User.Add
{
    public partial class InsertPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                this.CityDataBinding();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.AddUserData();

            this.HobbiesDataBinding();
        }

        protected void txtBirthDate_TextChanged(object sender, EventArgs e)
        {
             this.BindAgeCalculation();           
        }

    }
}