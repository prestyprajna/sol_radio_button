﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Radio_Button_Task.Models
{
    public class HobbyEntity
    {
        public decimal? HobbyId { get; set; }

        public string HobbyName { get; set; }
        
        public bool Interested { get; set; }

        public decimal? UserId { get; set; }
    }
}