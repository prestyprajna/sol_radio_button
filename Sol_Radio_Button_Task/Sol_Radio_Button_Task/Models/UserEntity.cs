﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Radio_Button_Task.Models
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool? Gender { get; set; }

        public string BirthDate { get; set; }

        public int? Age { get; set; }

        public decimal? CityId { get; set; }

        //public HobbyEntity hobbyEntityObj { get; set; }

        public List<HobbyEntity> hobbyEntityObj { get; set; }
    }
}