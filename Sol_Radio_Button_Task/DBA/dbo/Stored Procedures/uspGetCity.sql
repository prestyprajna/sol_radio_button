﻿CREATE PROCEDURE uspGetCity
(
	@Command VARCHAR(MAX),

	@CityId NUMERIC(18,0),
	@CityName VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='GetCityData'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT C.CityId,
				C.CityName
					FROM tblCity AS C
						
				SET @Status=1
				SET @Message='GetCityDataSuccessfull'

				COMMIT TRANSACTION
				
			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()

				SET @Status=0
				SET @Message='GetCityDataException'

				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH
		END

	END
