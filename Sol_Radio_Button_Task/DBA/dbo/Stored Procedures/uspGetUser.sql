﻿CREATE PROCEDURE uspGetUser
(
	@Command VARCHAR(MAX)=NOTNULL,

	@UserId NUMERIC(18,0)=NOTNULL,
	@FirstName VARCHAR(50)=NULL,
	@LastName VARCHAR(50)=NULL,
	--@BirthDate DATETIME,
	--@Age INT,
	--@Gender BIT,

	--@CityId NUMERIC(18,0),

	--@HobbyId NUMERIC(18,0),	
	--@HobbyName VARCHAR(50),
	--@Interested BIT,
	
	
	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
		
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='SELECT'
			BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserId,
				U.FirstName,
				U.LastName,
				U.BirthDate,
				U.Age,
				U.Gender,
				U.CityId
					FROM tblUser AS U
						WHERE U.FirstName=@FirstName
						AND
						U.LastName=@LastName

				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			END

		ELSE IF @Command='SELECT_USER_DATA_BY_ID'
			BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserId,
				U.FirstName,
				U.LastName,
				U.BirthDate,
				U.Age,
				U.Gender,
				U.CityId
					FROM tblUser AS U
						WHERE U.UserId=@UserId		


				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			END

		ELSE IF @Command='SELECT_HOBBIES_DATA_BY_ID'
			BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				
				SELECT H.HobbyId,
				H.HobbyName,
				H.Interested,
				H.UserId
					FROM tblHobbies AS H
						WHERE H.UserId=@UserId


				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			END

		

	END
