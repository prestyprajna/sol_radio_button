﻿CREATE PROCEDURE uspSetHobbies
(
	@Command VARCHAR(MAX),

	@HobbyId NUMERIC(18,0),	
	@HobbyName VARCHAR(50),
	@Interested BIT,
	@UserId NUMERIC(18,0),	

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	IF @Command='INSERT'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			INSERT INTO tblHobbies
			(
			HobbyName,
			Interested,
			UserId
			)
			VALUES
			(
			@HobbyName,
			@Interested,
			@UserId
			)

			SET @Status=1
			SET @Message='INSERT HOBBIES SUCCESSFULL'

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			
			SET @Status=0
			SET @Message='INSERT HOBBIES EXCEPTION'
			ROLLBACK TRANSACTION

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END
	

	END
