﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [Gender]    BIT          NULL,
    [BirthDate] DATETIME     NULL,
    [Age]       INT          NULL,
    [CityId]    NUMERIC (18) NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

