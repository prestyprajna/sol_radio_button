﻿CREATE TABLE [dbo].[tblHobbies] (
    [HobbyId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [HobbyName]  VARCHAR (50) NULL,
    [Interested] BIT          NULL,
    [UserId]     NUMERIC (18) NULL,
    PRIMARY KEY CLUSTERED ([HobbyId] ASC)
);

