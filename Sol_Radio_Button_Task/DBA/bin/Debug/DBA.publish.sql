﻿/*
Deployment script for RadioDb

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "RadioDb"
:setvar DefaultFilePrefix "RadioDb"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL11.PRAJNA\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL11.PRAJNA\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE,
                DISABLE_BROKER 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Altering [dbo].[uspGetUser]...';


GO
ALTER PROCEDURE uspGetUser
(
	@Command VARCHAR(MAX)=NOTNULL,

	@UserId NUMERIC(18,0)=NOTNULL,
	@FirstName VARCHAR(50)=NULL,
	@LastName VARCHAR(50)=NULL,
	@BirthDate DATETIME,
	@Age INT,
	@Gender BIT,

	@CityId NUMERIC(18,0),
	--@HobbyId NUMERIC(18,0),
	
	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
		
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='SELECT'
			BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserId,
				U.FirstName,
				U.LastName,
				U.BirthDate,
				U.Age,
				U.Gender,
				U.CityId
					FROM tblUser AS U
						WHERE U.FirstName=@FirstName
						AND
						U.LastName=@LastName

				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			END

		

	END
GO
PRINT N'Update complete.';


GO
